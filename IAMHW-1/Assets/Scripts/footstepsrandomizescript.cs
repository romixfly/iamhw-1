using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperFootstepsScript : MonoBehaviour
{
    public AudioSource footstepsAudioSource;
    public AudioClip[] FootstepsAudioClip;
    public int LastIndex;
    public int NewIndex;
    
    void Start()
    {
        footstepsAudioSource = gameObject.AddComponent<AudioSource>();
      
    }


    void footstep()
    {
        Randomization();
        footstepsAudioSource.volume = Random.Range(0.8f, 1f);
        footstepsAudioSource.pitch = Random.Range(0.8f, 1.2f);
        footstepsAudioSource.PlayOneShot(FootstepsAudioClip[NewIndex]);
        Debug.Log("Наступил " + NewIndex);
        LastIndex = NewIndex;
    }
 
    void Randomization()
    {
        NewIndex = Random.Range(0, FootstepsAudioClip.Length);
        while (NewIndex == LastIndex)
            NewIndex = Random.Range(0, FootstepsAudioClip.Length);
    }
}
